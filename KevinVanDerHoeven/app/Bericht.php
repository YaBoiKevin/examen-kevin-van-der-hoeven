<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Bericht extends Model
{
    protected function read() {

        $query = DB::table('berichten')
            ->select('id', 'titel', 'content')
            ->where('user_id', Auth::id())
            ->get()
            ->toArray();


        $result = json_decode(json_encode($query), true);
        return $result;
    }

    protected function addBericht($request){

        $titel = $request->input('titel');
        $content = $request->input('content');
        $user_id = Auth::id();

            DB::table('berichten')
            ->insert(['titel' => $titel, 'content' => $content, 'user_id' => $user_id]);
    }

    protected function editBericht($request){

        $titel = $request->input('titel');
        $content = $request->input('content');
        $id = $request->input('id');

        DB::table('berichten')
            ->where('id', $id)
            ->update(['titel' => $titel, 'content' => $content]);
    }

    protected function deleteBericht($request){

        $id = $request->input('id');

        DB::table('berichten')
            ->where('id', $id)
            ->delete();
    }
}
