<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Gebruiker extends Model
{
    protected function read() {
        $query = DB::table('gebruikers')
            ->select('id', 'gebruiker', 'email')
            ->where('id', '!=', Auth::id())
            ->get()
            ->toArray();

        $result = json_decode(json_encode($query), true);
        return $result;
    }

    protected function addGebruiker($gebruiker, $email, $wachtwoord){

        DB::table('gebruikers')
            ->insert(['gebruiker' => $gebruiker, 'email' => $email, 'wachtwoord' => $wachtwoord]);
    }

    protected function editGebruiker($request){

        $gebruiker = $request->input('gebruiker');
        $email = $request->input('email');
        $id = $request->input('id');

        DB::table('gebruikers')
            ->where('id', $id)
            ->update(['gebruiker' => $gebruiker, 'email' => $email]);

        DB::table('users')
            ->where('id', $id)
            ->update(['name' => $gebruiker, 'email' => $email]);
    }

    protected function deleteGebruiker($request){

        $id = $request->input('id');

        DB::table('gebruikers')
            ->where('id', $id)
            ->delete();

        DB::table('users')
            ->where('id', $id)
            ->delete();
    }
}
