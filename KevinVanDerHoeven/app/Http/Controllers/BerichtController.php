<?php

namespace App\Http\Controllers;

use App\Bericht;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class BerichtController extends Controller
{

    function index(){
        $berichten = Bericht::read();
        return view('berichten', ['berichten'=> $berichten]);
    }

    function addBericht(Request $request){
        Bericht::addBericht($request);
        return back();
    }

    function editBericht(Request $request){
        Bericht::editBericht($request);
        return back();
    }

    function deleteBericht(Request $request){
        Bericht::deleteBericht($request);
        return back();
    }
}
