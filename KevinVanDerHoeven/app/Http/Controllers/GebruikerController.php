<?php

namespace App\Http\Controllers;

use App\Gebruiker;
use Illuminate\Http\Request;

class GebruikerController extends Controller
{
    function index(Request $request){
        $gebruikers = Gebruiker::read();
        return view('gebruikers', ['gebruikers'=> $gebruikers]);
    }

    function addGebruiker(Request $request){
        Gebruiker::addGebruiker($request);
        return back();
    }

    function editGebruiker(Request $request){
        Gebruiker::editGebruiker($request);
        return back();
    }

    function deleteGebruiker(Request $request){
        Gebruiker::deleteGebruiker($request);
        return back();
    }
}
