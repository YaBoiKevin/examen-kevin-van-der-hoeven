@extends('layouts.app')
@section('content')
    @auth
        <div class="container col-lg-10">
            <div class="row">
                @foreach($berichten as $bericht)
                    <form class="col-lg-12" method="POST" action="/berichten/aanpassen?id={{  $bericht['id'] }}">
                        @csrf
                        <div style="text-align: center" class="row">
                            <label class="col-lg-4" for="titel">Titel:</label>
                            <label class="col-lg-4" for="content">Content:</label>
                        </div>
                        <div class="row">
                            <textarea id="titel" name="titel" class="col-lg-4" required>{{ $bericht['titel'] }}</textarea>
                            <textarea id="content" name="content" class="col-lg-4" required>{{ $bericht['content'] }}</textarea>
                            <div class="col-lg-4">
                                <input style="margin: 0 0 10px;" name="submit_edit" value="Aanpassen" type="submit" class="col-lg-6 btn btn-primary" />
                    </form>
                    <form method="POST" action="/berichten/verwijderen?id={{  $bericht['id'] }}">
                        @csrf
                        <input name="submit_delete" value="Verwijderen" type="submit" class="col-lg-6 btn btn-danger" />
                    </form>
                </div>
            </div>
            @endforeach


            </div>
            <div class="row">
                <form class="col-lg-12" method="POST" action="/berichten/toevoegen">
                    @csrf
                    <div style="text-align: center" class="row">
                        <label class="col-lg-4" for="titel">Titel:</label>
                        <label class="col-lg-4" for="content">Content:</label>
                    </div>
                    <div class="row">
                        <textarea id="titel" name="titel" class="col-lg-4" required></textarea>
                        <textarea id="content" name="content" class="col-lg-4" required></textarea>
                        <div class="col-lg-4">
                            <input style="margin: 20px 0 25px;" name="submit_add" value="Toevoegen" type="submit" class="col-lg-6 btn btn-success" />
                        </div>
                    </div>

                </form>
            </div>
        </div>
    @endauth
    @guest
        <h2 style="text-align: center">U moet ingelogd zijn om deze pagina te kunnen betreden!</h2>
    @endguest
@endsection
