@extends('layouts.app')
@section('content')
    @auth

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">{{ __('Register') }}</div>

                        <div class="card-body">
                            <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                                @csrf

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                    <div class="col-md-6">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-success">
                                            {{ __('Toevoegen') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="container col-lg-10" style="margin-top: 20px;">
            @foreach($gebruikers as $gebruiker)

            <form class="col-lg-12" method="POST" action="/gebruikers/aanpassen?id={{  $gebruiker['id'] }}">
                @csrf
                <div class="row">
                    <label class="col-lg-4" for="editname">Gebruiker:</label>
                    <label class="col-lg-4" for="editemail">Email:</label>
                </div>
                <div class="row">
                    <input id="editname" type="text" class="col-lg-4 form-control" name="gebruiker" value="{{ $gebruiker['gebruiker'] }}" required>
                    <input id="editemail" type="email" class="col-lg-4 form-control" name="email" value="{{ $gebruiker['email'] }}" required>

                    <div class="col-lg-4">
                        <input style="margin: 0 0 10px;" name="submit_edit" value="Aanpassen" type="submit" class="col-lg-6 btn btn-primary" />
            </form>
                        <form method="POST" action="/gebruikers/verwijderen?id={{  $gebruiker['id'] }}">
                            @csrf
                            <input name="submit_delete" value="Verwijderen" type="submit" class="col-lg-6 btn btn-danger" />
                        </form>
                    </div>
                </div>

            @endforeach
        </div>
    @endauth
    @guest
        <h2 style="text-align: center">U moet ingelogd zijn om deze pagina te kunnen betreden!</h2>
    @endguest
@endsection
