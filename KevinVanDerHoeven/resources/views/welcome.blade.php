@extends("layouts.app")

@section("content")
    @guest
    <h2 style="text-align: center"> Welkom! Indien u nog geen account heeft kunt u er een aanvragen, </h2>
    <h2 style="text-align: center"> Indien u al een account heeft kunt u ook inloggen via de inlogpagina! </h2>
    @endguest
    @auth
        <h2 style="text-align: center">Welkom! U kunt berichten aanmaken en aanpassen bij de berichtenpagina,</h2>
        <h2 style="text-align: center">ook kunt u gebruikers aanpassen!</h2>
    @endauth
@endsection