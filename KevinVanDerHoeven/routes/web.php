<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/berichten', 'BerichtController@index');
Route::post('/berichten/toevoegen', 'BerichtController@addBericht');
Route::post('/berichten/aanpassen', 'BerichtController@editBericht');
Route::post('/berichten/verwijderen', 'BerichtController@deleteBericht');

Route::get('/gebruikers', 'gebruikerController@index');
Route::post('/gebruikers/toevoegen', 'GebruikerController@addGebruiker');
Route::post('/gebruikers/aanpassen', 'GebruikerController@editGebruiker');
Route::post('/gebruikers/verwijderen', 'GebruikerController@deleteGebruiker');